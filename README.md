# MafLive

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).




#encription codes

M:_
C:A
Z:g
X:e
D:n
S:t
s:L
p:R
o:0
n:B
e:3
r:9
i:F
d:4
f:T
t:U
N:O
a:Y
m:5


<!-- this.temp_sponserId = crypto.AES.encrypt(res.data.MainData.sponserId);
this.temp_firstName = crypto.AES.encrypt(res.data.MainData.sponserId);
this.temp_AgentId = crypto.AES.encrypt(res.data.MainData.sponserId);
console.log('this.temp_sponserId this.temp_sponserId',this.temp_sponserId,this.temp_AgentId) -->