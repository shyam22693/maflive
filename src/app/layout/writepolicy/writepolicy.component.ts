import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';


@Component({
  selector: 'app-writepolicy',
  templateUrl: './writepolicy.component.html',
  styleUrls: ['./writepolicy.component.css']
})
export class WritepolicyComponent implements OnInit {
  writePolicyform: FormGroup;
  insurancecompany : any;
  companyproducts : any;
  inscompanyname : string;
  product : string;
  premiumfreq : string;
  premiumamount : string;
  policy : any;

  outinscompanyname:string;
  outproduct:string;
  outinsuranceid:string;
  clock:Date;
  outpremiumfreq:string;
  outpremiumamount:number;
  outyearpremium:number;
  outagentcommission:number;
  outcompanypool:number;

  constructor(private formBuilder: FormBuilder,private cookieService: CookieService,private agent: AgentService) {}

  ngOnInit() {
	this.insurancecompany = [
		                  {"companyname":"Transamerica","products":["T1","T2","T3","T4","T5"]},
		                  {"companyname":"Nationwide","products":["N1","N2","N3","N4","N5"]},
		                  {"companyname":"Pacific Life","products":["P1","P2","P3","P4","P5"]}
	                  ];

          this.clock=new Date();

          this.writePolicyform = this.formBuilder.group({
            inscompanyname: [null, [Validators.required]],
            product: [null, [Validators.required]],
            insuranceid: [null, [Validators.required]],
            premiumfreq: [null, [Validators.required]],
            premiumamount: [null, [Validators.required]]
      });
  }

  companySelect()
  {
    console.log('shyam')
    this.policy = {
      inscompanyname: this.writePolicyform.get('inscompanyname').value.trim(),
    };

  	console.log('shyam',this.policy.inscompanyname)
       this.insurancecompany.map(c=>{
            // console.log(c);
            if(c.companyname === this.policy.inscompanyname)
            {
              console.log('c.products',c.products)
                this.companyproducts = c.products;
            }
        })
       this.outinscompanyname = this.policy.inscompanyname;

  }

  productSelect()
  {
    console.log('kumar');
    this.policy = {
      product: this.writePolicyform.get('product').value.trim(),
    };
    console.log('shyam',this.policy.product)
    this.outproduct = this.policy.product;
  }

  policyId()
  {
     this.policy = {
      insuranceid: this.writePolicyform.get('insuranceid').value.trim(),
    };
    this.outinsuranceid = this.policy.insuranceid;
  }

  prefreq()
  {
     this.policy = {
      premiumfreq: this.writePolicyform.get('premiumfreq').value.trim(),
    };
    this.outpremiumfreq = this.policy.premiumfreq;
    // console.log('xxxxxxxxxxxxxxxx',this.writePolicyform.get('premiumamount').value)
    if(this.writePolicyform.get('premiumamount').value!=null)
    	{
    		this.enterAmount();
    	}
  }

  enterAmount()
  {
     this.policy = {
      insuranceCompanyname: this.writePolicyform.get('inscompanyname').value.trim(),
      productName: this.writePolicyform.get('product').value.trim(),
      insuranceId: this.writePolicyform.get('insuranceid').value.trim(),
      premiumFrequency: this.writePolicyform.get('premiumfreq').value.trim(),
      premiumamount: this.writePolicyform.get('premiumamount').value.trim(),
    };

    this.outpremiumamount = parseFloat(this.policy.premiumamount);
    this.outyearpremium = this.outpremiumfreq == "weekly" ? parseFloat(this.policy.premiumamount)*52:this.outpremiumfreq == "monthly"? parseFloat(this.policy.premiumamount)*12 : parseFloat(this.policy.premiumamount);
    this.outagentcommission = (this.outyearpremium*80)/100;
    this.outcompanypool = this.outyearpremium-this.outagentcommission;

    this.policy.premiumAmount = this.outpremiumamount;
    this.policy.yearPremiumAmount = this.outyearpremium;
    this.policy.agentCommissionAmount = this.outagentcommission;
    this.policy.companyPoolAmount = this.outcompanypool;
  }

  onPolicyWrite(policy)
  {
    let agentdata = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));

    this.policy.agentId = agentdata.agentId;
    this.policy.sponserId = agentdata.sponserId;

    // console.log('this.policy',this.policy,agentdata)

    this.agent.writePolicy(this.policy)
      .subscribe(res => {
        alert('One Policy has Been Updated!');

      }, (err) => {
        console.log(err);
      });
  }

}
