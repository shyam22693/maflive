import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WritepolicyComponent } from './writepolicy.component';

describe('WritepolicyComponent', () => {
  let component: WritepolicyComponent;
  let fixture: ComponentFixture<WritepolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WritepolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WritepolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
