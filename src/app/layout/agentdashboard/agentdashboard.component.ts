import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AdminauthorizationService } from '../../customservices/adminauthorization.service';
import { AuthorizationService } from '../../customservices/authorization.service';



const poolData = {
  UserPoolId: 'us-east-1_mHJWVEl15', // Your user pool id here
  ClientId: '3ahsjgh8mrhf4dvgfsoujaaisl' // Your client id here  
};

const userPool = new CognitoUserPool(poolData);


@Component({
  selector: 'app-agentdashboard',
  templateUrl: './agentdashboard.component.html',
  styleUrls: ['./agentdashboard.component.css']
})
export class AgentdashboardComponent implements OnInit {

    acceptedMimeTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/jpg'];


   acceptedLicenceMimeTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/jpg',
    'application/pdf'];





  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('fileLicenceInput') fileLicenceInput: ElementRef;


  url = '';
  agentDetails: any;
  fileDataUri: string | ArrayBuffer;
  fileLicenceDataUri: string | ArrayBuffer;
  errorMsg: string;
  errorLicenceMsg: string;
  fileName:string;
  fileLicenceName:string;
  fileType:string;
  fileLicenceType:string;
  imageUpload:boolean;
  imageLicenceUpload:boolean;

  constructor(private agent: AgentService, private agentAuth: AuthorizationService,
    private router: Router, private cookieService: CookieService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
      // location.reload();
      // this.fileDataUri = '';
      this.errorMsg = '';
      this.errorLicenceMsg = '';
      this.imageUpload = false;
      this.imageLicenceUpload = false;
      let id = null;

    const agentId = localStorage.getItem('CognitoIdentityServiceProvider.4gkgpcug955h6kfapt4n61ojkr.LastAuthUser');
    console.log('agentId agentId', agentId);
    const gatData = this.cookieService.get('AXRGBSYEQWATNLKFSYT')? JSON.parse(this.cookieService.get('AXRGBSYEQWATNLKFSYT')): null;
    id = gatData!=null ? gatData.agentId : null;

      if(id!=null)
      {

        this.agent.getAgentProfile(id)
          .subscribe(res => {
            console.log('resr res',res);
            console.log('data is on');
            this.agentDetails = res.data;
            this.cookieService.set('AHJSKIDGTERCDHDFCSKDHDGDT',JSON.stringify(res.data));
          }, (err) => {
            console.log(err);
          });
      }
      else{
        this.router.navigateByUrl('/');
      }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (Event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      };
    }
  }
  // Documents Upload

  // uploadFile(event) {

  //   const files: FileList = event.target.files;
  //   console.log('event', files);
  //   if (files.length == 0) {
  //     console.log('File', files);
  //     return

  //   }
  // }

  agentLogout() {
    console.log('shyam');
    this.agentAuth.logOut();
    this.router.navigateByUrl('/home');
    this.cookieService.delete('AHJSKIDGTERCDHDFCSKDHDGDT');
  }



  previewFile() {
    const file = this.fileInput.nativeElement.files[0];

    console.log('file file',file);

    if (file && this.validateFile(file)) {
      this.fileName = file.name;
      this.fileType = file.type;
      this.imageUpload = true;
      const reader = new FileReader();
      reader.readAsDataURL(this.fileInput.nativeElement.files[0]);
      reader.onload = () => {
        this.fileDataUri = reader.result;
      }
    } else {
      this.errorMsg = 'File must be jpg, jpeg, or png and cannot be exceed 500 KB in size'
    }
  }

  uploadFile(event) {
    event.preventDefault();
    if (this.fileDataUri!=undefined){
      const base64File = this.fileDataUri.toString().split(',')[1];

      // console.log(base64File)

      let agentdata = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));

       // console.log(agentdata)

      let data = {base64File : base64File,agentId:agentdata.agentId,type:"image",fileType:this.fileType,fileName:this.fileName}

      // console.log('data',data);
      // console.log('i am good');
     this.agent.uploadProfilePic(data)
      .subscribe(res => {
          alert('Image Uploaded Successfully!')
      }, (err) => {
        console.log(err);
      });    
    }

  }

  validateFile(file) {
    return this.acceptedMimeTypes.includes(file.type) && file.size < 5000000;
  }


  validateLicenceFile(file) {
    return this.acceptedLicenceMimeTypes.includes(file.type) && file.size < 5000000;
  }






    previewLicenceFile() {
    const file = this.fileLicenceInput.nativeElement.files[0];

    console.log('file file',file);

    if (file && this.validateLicenceFile(file)) {
      this.fileLicenceName = file.name;
      this.fileLicenceType = file.type;
      this.imageLicenceUpload = true;
      const reader = new FileReader();
      reader.readAsDataURL(this.fileLicenceInput.nativeElement.files[0]);
      reader.onload = () => {
        this.fileLicenceDataUri = reader.result;
      }
    } else {
      this.errorLicenceMsg = 'File must be jpg, jpeg, png, or pdf and cannot be exceed 500 KB in size'
    }
  }

  uploadLicenceFile(event) {
    event.preventDefault();
    if (this.fileLicenceDataUri!=undefined){
      const base64LicenceFile = this.fileLicenceDataUri.toString().split(',')[1];

      let agentdata = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));

      let dataLicence = {base64File : base64LicenceFile,agentId:agentdata.agentId,type:"licence",fileType:this.fileLicenceType,fileName:this.fileLicenceName}


      console.log('dataLicence',dataLicence);
     this.agent.uploadAgentLicence(dataLicence)
      .subscribe(res => {
          alert('Licence Uploaded Successfully!')
      }, (err) => {
        console.log(err);
      });    
    }

  }

    writePol()
    {
      this.router.navigateByUrl('/agent-write-policy')
    }
}
