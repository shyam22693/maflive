import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-autoa',
  templateUrl: './autoa.component.html',
  styleUrls: ['./autoa.component.css']
})
export class AutoaComponent implements OnInit {
  placeSearch;
  autocomplete;
  public google;

  componentForm: any;
  constructor() { }

  ngOnInit() {

  this.componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };


  }
  initAutocomplete() {
    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    this.autocomplete = new this.google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), { types: ['geocode'] });

    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
    this.autocomplete.setFields('address_components');

    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    this.autocomplete.addListener('place_changed', this.fillInAddress());
  }
  fillInAddress() {
    // Get the place details from the autocomplete object.
    const place = this.autocomplete.getPlace();

    for (const component of this.componentForm) {
      (<HTMLInputElement>document.getElementById(component)).value = '';
      (<HTMLInputElement>document.getElementById(component)).disabled = false;
    }

    // Get each component of the address from the place details,
    // and then fill-in the corresponding field on the form.
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      if (this.componentForm[addressType]) {
        const val = place.address_components[i][this.componentForm[addressType]];

        (<HTMLInputElement>document.getElementById(addressType)).value = val;
      }
    }
  }

  geolocate() {
    console.log('geolocate');
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        const geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        const circle = new this.google.maps.Circle(
          { center: geolocation, radius: position.coords.accuracy });
        this.autocomplete.setBounds(circle.getBounds());
      });
    }
  }
}
