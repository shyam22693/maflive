import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoaComponent } from './autoa.component';

describe('AutoaComponent', () => {
  let component: AutoaComponent;
  let fixture: ComponentFixture<AutoaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
