import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminagentbyidComponent } from './adminagentbyid.component';

describe('AdminagentbyidComponent', () => {
  let component: AdminagentbyidComponent;
  let fixture: ComponentFixture<AdminagentbyidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminagentbyidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminagentbyidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
