import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import {AuthorizationService} from "../../customservices/authorization.service";


@Component({
  selector: 'app-adminagentbyid',
  templateUrl: './adminagentbyid.component.html',
  styleUrls: ['./adminagentbyid.component.css']
})
export class AdminagentbyidComponent implements OnInit {
showTable:boolean;
  submitted = false;
  rejectPopup = false;
  loader = true;
  agentDetail: any;
  rejectform: FormGroup;

  constructor(private agent: AgentService,private agentAuth: AuthorizationService, private router: Router,private cookieService: CookieService,private activatedRoute: ActivatedRoute,private formBuilder: FormBuilder) { }

  ngOnInit() {
    // Reject Form
    this.rejectform = this.formBuilder.group({
      comments: [null, [Validators.required]],
      emailId: [null, [Validators.required]],
      agentId: [null, [Validators.required]],
    });
    const agentId = this.activatedRoute.snapshot.paramMap.get('agentId');
    this.agent.getAgentDetails(agentId)
      .subscribe(res => {
        this.loader = false;
        this.agentDetail = res.data.Item;
        console.log('this.agentDetail this.agentDetail',this.agentDetail)
      }, (err) => {
        console.log(err);
      });
  }


 makePassword() {
  var text = "";
  var possible = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789";

  for (var i = 0; i < 9; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}

  onAgentAccepts(agentIds,firstName,emailId)
  {

    let passwords = this.makePassword();
    var data={agentId:agentIds,firstName:firstName,emailId:emailId,password:passwords};

    console.log('data data',data);

    let email = data.emailId;
    let password = data.password;
    let agentid = data.agentId;

      this.agentAuth.register(email,password,agentid).subscribe(
      (output) => {
          this.onAccepting(data)
        },
        (err) => {
           console.log(err);
      });
      }

      onAccepting(data)
      {
         this.agent.onAgentAccepted(data)
          .subscribe(res => {
          console.log('done!')
          alert('Verification is Successful. Login Credentials with Send via Email!');
          }, (err) => {
          console.log(err);
          alert('Something Went Wrong.Please try again later');
        });
      }

  // Reject Method

  onAgentReject() {
    this.rejectPopup = true;
  }
  rfclose() {
    this.rejectPopup = false;
  }
  // Get form
  get f() { return this.rejectform.controls; }
  // Reject Form Submit
  onReject(email,agentid,firstname,user) {
    console.log('user',email,agentid,firstname,user);
    let data = {emailId:email,agentId:agentid,firstName:firstname,comment:user.comments};
    console.log("data",data);
    if(data.emailId!=null && data.agentId!=null && data.firstName!=null && data.comment!=null)
    {
      this.regectSendMail(data);
          this.submitted = true;
    }

  }

  regectSendMail(data)
  {
    this.agent.onRejectEmail(data)
      .subscribe(res => {
      console.log('done!')
      alert('Email Had Been Sent To The Agent!');
      }, (err) => {
      console.log(err);
      alert('Something Went Wrong.Please try again later');
    });
  }


}
