import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm } from '@angular/forms';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import { AuthorizationService } from '../../customservices/authorization.service';

import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

const poolData = {
  UserPoolId: 'us-east-1_B5waepwSZ', // Your user pool id here
  ClientId: '4gkgpcug955h6kfapt4n61ojkr' // Your client id here
};

const userPool = new CognitoUserPool(poolData);



declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  input: any;
  public visibilityIcon: Boolean = true;
  public visibilityStrikeIcon: Boolean = false;
  public pass: Boolean = true;
  public passtext: Boolean = false;
  signUpModal: Boolean = false;
  confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  error: string = '';
  agentId: string;
  submitted = false;
  user: any;
  agentloginform: FormGroup;
  bAuthenticated = false;
  constructor(private formBuilder: FormBuilder, private agentAuth: AuthorizationService,
    public router: Router, private activatedRoute: ActivatedRoute, private cookieService: CookieService) { }

  ngOnInit() {
    // Nav

    const selector = '.nav li';

    $(selector).on('click', function () {
      $(selector).removeClass('active');
      $(this).addClass('active');
    });

    this.agentloginform = this.formBuilder.group({
      agentId: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });


    let authenticatedUser = this.agentAuth.getAuthenticatedUser();

    console.log('the absolue username', localStorage.getItem('CognitoIdentityServiceProvider.4gkgpcug955h6kfapt4n61ojkr.LastAuthUser'))

    if (authenticatedUser == null) {
      return;
    }
    this.bAuthenticated = true;
  }
  get ad() { return this.agentloginform.controls; }
  onLogin(user) {
    // $('.signUpModal').modal('data-toggle');

    this.user = {
      agentid: this.agentloginform.get('agentId').value,
      password: this.agentloginform.get('password').value
    };

    console.log('cccc', this.user);
    this.submitted = true;
    console.log('Register', this.user);
    if (this.agentloginform.invalid) {
      console.log('please fill all fields');
      return;
    }

    let agentid = this.user.agentid;
    let password = this.user.password;

    console.log('cccccccccccccccc', agentid, password);

    this.agentAuth.signIn(agentid, password).subscribe(
      (data) => {
        // $('.login-btn').attr('data-dismiss', 'modal');
        this.confirmCode = false;
        let cognitoUser = userPool.getCurrentUser();
        if (cognitoUser != null) {
          let cognitoUser = userPool.getCurrentUser();
          let agentid = 'dummy';
          cognitoUser.getSession((err, session) => {
            cognitoUser.getUserAttributes((err, result) => {
              console.log('*************', result[0].getValue())
              // this.agentId = result[0].getValue();
              // var agentId = result[0].getValue();
              if (result[0].getValue()) {
                let agentId = { "agentId": result[0].getValue() };
                this.cookieService.set('AXRGBSYEQWATNLKFSYT', JSON.stringify(agentId));
                //  $('button').attr("data-dismiss","modal");  
                this.router.navigateByUrl('/agent-profile')
              }
            })
          });
        }
      },
      (err) => {
        console.log(err);
        alert('Email ID or Password Mismatch!')
        this.error = 'Registration Error has occurred';
      }
    );
  }
  // Go oto register
  goToRegister() {
    // $('.signUpModal').modal('toggle');
    this.router.navigateByUrl('/register');

  }
  /**Password Icons */

  visibiltyPassword() {
    this.pass = false;
    this.passtext = true;
    this.visibilityIcon = false;
    this.visibilityStrikeIcon = true;
  }

  visibilityString() {
    this.pass = true;
    this.passtext = false;
    this.visibilityIcon = true;
    this.visibilityStrikeIcon = false;
  }
  /** Menu */
  mobileMenu() {
    console.log('test');
  }


}
