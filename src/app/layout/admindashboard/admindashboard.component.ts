import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { AgentService } from '../../../agent.service';

import { AdminauthorizationService } from "../../customservices/adminauthorization.service";


@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit {
  public loader: Boolean = false;
  public agents = [];
  agentId: string;

  constructor(private formBuilder: FormBuilder, private agent: AgentService, private adminAuth: AdminauthorizationService, private router: Router, private cookieService: CookieService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loader = true;
    this.agent.getAllAgent()
      .subscribe(res => {
        this.agents = res.data.MainData.Items;
        this.loader = false;
        console.log('agents', this.agents);
      }, (err) => {
        console.log(err);
      });

  }

  getAgentById(agentId) {
    console.log('agentid', agentId);
    this.agentId = agentId;
    this.router.navigate(['/admin-agent', agentId]);
  }

  adminLogout() {
    console.log('shyam');
    this.adminAuth.logOut();
    this.router.navigateByUrl('/admin');
  }
}
