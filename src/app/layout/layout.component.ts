import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  hideElement = false;
  constructor( public router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/payment' || event.url === '/forgotpassword' ) {
          this.hideElement = true;
        } else {
          this.hideElement = false;
        }
      }
    });
  }

  ngOnInit() {
  }

}
