import { AfterViewInit, Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { of } from 'rxjs';

import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from '../../../projects/ngx-paypal-lib/src/public_api';

import { AgentService } from '../../../agent.service';

import { CookieService } from 'ngx-cookie-service';


// declare var hljs: any;


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  public loader: Boolean = false;
  public payPalConfig?: PayPalConfig;

  firstName: string;
  emailId: string;
  phoneNo: string;
  agentId: string;
  tempAgentData: any;


  constructor(private router: Router, private agentApi: AgentService, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.initConfig();
    this.tempAgentData = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));
    this.firstName = this.tempAgentData.firstName;
    this.emailId = this.tempAgentData.emailId;
    this.phoneNo = this.tempAgentData.phoneNo;
    console.log('this.tempAgentData',this.tempAgentData);
  }

  ngAfterViewInit(): void {
    // this.prettify();
  }

  private initConfig(): void {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
    }, 10000);
    this.payPalConfig = new PayPalConfig(
      PayPalIntegrationType.ClientSideREST,
      PayPalEnvironment.Sandbox,
      {
        commit: true,
        client: {
          sandbox:
            'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R'
        },
        button: {
          label: 'paypal',
          layout: 'vertical'
        },
        onAuthorize: (data, actions) => {
          console.log('Authorize');
          return of(undefined);
        },
        onPaymentComplete: (data, actions) => {

        console.log('tempAgentData tempAgentData', this.tempAgentData);

          let final_data = new Object();
          this.tempAgentData.amount = '$100';
          this.tempAgentData.paymentToken = data.paymentToken;
          this.tempAgentData.intent = data.intent;
          this.tempAgentData.orderId = data.orderID;
          this.tempAgentData.payerId = data.payerID;
          this.tempAgentData.paymentId = data.paymentID;
          this.tempAgentData.amount = '$100';
          this.tempAgentData.isDocSigned = true;
          final_data = this.tempAgentData;

          console.log('final_data final_data', final_data);
          this.agentApi.postPayment(final_data)
            .subscribe(res => {
              this.router.navigate(['/thankyou']);
            }, (err) => {
              console.log(err);
              // alert('Something Went Wrong.Please try Again!');
              this.router.navigate(['/paymentfail']);
            });
        },
        onCancel: (data, actions) => {
          console.log('OnCancel');
        },
        onError: err => {
          console.log('OnError');
        },
        onClick: () => {
          console.log('onClick');
        },
        validate: (actions) => {
          console.log(actions);
        },
        experience: {
          noShipping: true,
          brandName: 'MAF PAYMEMTS'
        },
        transactions: [
          {
            amount: {
              total: 100.00,
              currency: 'USD',
              details: {
                subtotal: 100.00,
                tax: 0.00,
                shipping: 0.00,
                handling_fee: 0.00,
                shipping_discount: 0.00,
                insurance: 0.00
              }
            },
            custom: 'Custom value',
            item_list: {
              items: [
                {
                  name: 'MAF SUBSCRIPTION',
                  description: 'THE AMOUNT FOR MAF SUBSCRIPTION.',
                  quantity: 1,
                  price: 100,
                  tax: 0.00,
                  sku: '1',
                  currency: 'USD'
                }],
              shipping_address: {
                recipient_name: 'Brian Robinson',
                line1: '4th Floor',
                line2: 'Unit #34',
                city: 'San Jose',
                country_code: 'US',
                postal_code: '95131',
                phone: '011862212345678',
                state: 'CA'
              },
            },
          }
        ],
        payment: () => {
          // create your payment on server side
          return of(undefined);
        },
        note_to_payer: 'Contact us if you have troubles processing payment'
      }
    );
  }

  // private prettify(): void {
  //   hljs.initHighlightingOnLoad();
  // }
  goToHome() {
    this.router.navigateByUrl('/home');
  }
}
