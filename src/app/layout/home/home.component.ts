import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  loader: Boolean = false;
  constructor() {
    setTimeout(function () {
      this.loader = true;
    }, 3000);
   }

  ngOnInit() {
    $('.carousel').carousel({
      interval: 5000
    });

    const timer = $('.timer');
    if (timer.length) {
      timer.appear(() => {
        timer.countTo();
      });
    }
  }
  // Counter


}
