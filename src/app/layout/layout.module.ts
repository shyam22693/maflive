import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { PaymentComponent } from './payment/payment.component';
import { ThankuComponent } from './thanku/thanku.component';
import { AboutComponent } from './about/about.component';
import { BecomeagentComponent } from './becomeagent/becomeagent.component';
import { ContactusComponent } from './contactus/contactus.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { AgentdashboardComponent } from './agentdashboard/agentdashboard.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { AdminsignupComponent } from './adminsignup/adminsignup.component';

import { AdminagentbyidComponent } from './adminagentbyid/adminagentbyid.component';

import { NgxPayPalModule } from '../../projects/ngx-paypal-lib/src/public_api';

// import {AuthorizationService} from "../customservices/authorization.service";

import { NgxSpinnerModule } from 'ngx-spinner';
import { AutoaComponent } from './autoa/autoa.component';
import { PaymentfailComponent } from './paymentfail/paymentfail.component';
import { WritepolicyComponent } from './writepolicy/writepolicy.component';

@NgModule({
  declarations: [LayoutComponent, HomeComponent, HeaderComponent,
    FooterComponent, RegisterComponent, PaymentComponent, ThankuComponent,
    AboutComponent, BecomeagentComponent, ContactusComponent, LoginComponent,
    AdminComponent, AdmindashboardComponent, AdminsignupComponent, AdminagentbyidComponent,
    AgentdashboardComponent, AutoaComponent, PaymentfailComponent, WritepolicyComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPayPalModule,
    NgxSpinnerModule,
    // ShowHidePasswordModule
  ],
  exports: [LayoutComponent,
    HomeComponent, HeaderComponent, FooterComponent, RegisterComponent,
    PaymentComponent, ThankuComponent,
    AboutComponent, BecomeagentComponent, ContactusComponent, LoginComponent,
    AdminComponent, AdmindashboardComponent, AdminsignupComponent,
     AdminagentbyidComponent, AgentdashboardComponent, AutoaComponent, PaymentfailComponent
  ]
})
export class LayoutModule { }
