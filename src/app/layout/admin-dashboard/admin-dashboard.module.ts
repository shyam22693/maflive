import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminDashboardRoutingModule } from './admin-dashboard-routing.module';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ExtendedTablesComponent } from './extended-tables/extended-tables.component';

@NgModule({
  imports: [
    CommonModule,
    AdminDashboardRoutingModule
  ],
  declarations: [AdminDashboardComponent, HeaderComponent, DashboardComponent, ExtendedTablesComponent],
  exports: [AdminDashboardComponent, HeaderComponent, DashboardComponent, ExtendedTablesComponent]
})
export class AdminDashboardModule { }
