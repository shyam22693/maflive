import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-paymentfail',
  templateUrl: './paymentfail.component.html',
  styleUrls: ['./paymentfail.component.css']
})
export class PaymentfailComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  goPayment() {
this.router.navigateByUrl('/payment');
  }
}
