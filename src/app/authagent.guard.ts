import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthorizationService } from './customservices/authorization.service';

import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthagentGuard implements CanActivate {
  href: any;
  cokieAgent: any;
  urlString: string;

  constructor(
    private auth: AuthorizationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService
  ) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {



      let authenticatedUser = this.auth.getAuthenticatedUser();
      let intruderLocalChange = this.auth.getFalseOnIntruder();

      console.log('intruderLocalChange',intruderLocalChange)

      if (authenticatedUser == null) {
        // redirect the user
        this.cookieService.delete('AXRGBSYEQWATNLKFSYT');
        this.router.navigate(['/']);
        return false;
      }
      else if(intruderLocalChange!=true)
      {

        this.cookieService.delete('AXRGBSYEQWATNLKFSYT');
        this.router.navigate(['/']);
        return false;
      }
      else{
      	return true;
      }
  }
}
