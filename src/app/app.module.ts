import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AgentService } from '../agent.service';
import { AuthorizationService } from './customservices/authorization.service';
import { AdminauthorizationService } from './customservices/adminauthorization.service';

import { NgxSpinnerModule } from 'ngx-spinner';

import { CookieService } from 'ngx-cookie-service';

import { AuthGuard } from './auth.guard';

import { AuthagentGuard } from './authagent.guard';

import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

import { AngularMaterialComponentsModule } from './angular-material-components/angular-material-components.module';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    ShowHidePasswordModule,
    NgbModule,
    ToastrModule.forRoot(), // ToastrModule added
    AngularMaterialComponentsModule
  ],
  providers: [AgentService, AdminauthorizationService, CookieService, AuthorizationService, AuthGuard, AuthagentGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
